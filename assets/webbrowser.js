import React, { Component } from 'react';
import { View, TextInput, Button, WebView, BackHandler } from 'react-native';

export default class InviteCode extends Component {
  constructor(props) {
    super(props);
    this.state = { 
        text: '',
        backButtonEnabled: false,
        ref:null,
     };
  }
  loadWeb = ()=>{
   
  }
componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.backHandler);
}

backHandler = () => {
    this.setState({backButtonEnabled:true});
    if(this.state.backButtonEnabled) {
        // alert(this.state.ref);
        this.state.ref.goBack();
        return true;
    }
}

  render() {
      if(this.state.text){
          return(
        <WebView
        userAgent="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"
        allowFileAccess
        javaScriptEnabled
        ref={(webView) => { this.state.ref = webView; }}
        source={{uri: 'https://www.squarecard.in/builder'}}
        style={{marginTop: 20}}
        />
          )
      }
    return (
    <View style={{flex:1,}}>
       
        <View style={{marginBottom:10,}}>
            <TextInput
                style={{height: 40, borderBottomColor:'#000', borderBottomWidth:1, }}
                placeholder ={'Enter the invite code here'}
                onChangeText={(text) => this.setState({text})}
                value={this.state.text}
            />
       </View>

       <Button 
        style={{marginTop:20,}}
        onPress={this.loadWeb}
        title="Show Card"
        color="#f6a38c"
        accessibilityLabel="Enter Invite Code for viewing card"
        />
    </View>
    );
  }
}
