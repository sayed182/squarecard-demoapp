import React from "react";
import { View, Text } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

import HomeScreen from "./screens/HomeScreen";
import DetailScreen from "./screens/DetailScreen";
import Dashboard from "./screens/Dashboard";

export default class App extends React.Component {
  render() {
      return <AppContainer />;
  }
}

const AppNavigator = createStackNavigator({
  Home: HomeScreen,
  Details: DetailScreen,
  Dashboard: Dashboard
},{
  initialRouteName: "Home"
});

const AppContainer = createAppContainer(AppNavigator);
