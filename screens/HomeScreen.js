import React from "react";
import { View, Text, Button } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
}
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent:"center",backgroundColor:"#fdf1ed",}}>
       
        <View style={{marginBottom:20, width:300}}>
          <Button
            title="Join Event"
            onPress={() => this.props.navigation.navigate('Details')}
            color="#f6a38c"
          />
        </View>
        <View style={{width:300}}>
        <Button
          title="Go to Dashboard"
          onPress={() => this.props.navigation.navigate('Dashboard')}
          color="#f6a38c"
        />
        </View>
        
      </View>
    );
  }
}

export default HomeScreen;