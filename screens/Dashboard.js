import React, { Component } from 'react'
import { Text, View, Button, WebView, BackHandler  } from 'react-native'

export class Dashboard extends Component {
static navigationOptions = {
    header: null,
}
constructor(props) {
    super(props);
    this.state = { 
        text: '',
        backButtonEnabled: false,
        ref:null,
        };
    }
componentDidMount(){
    // BackHandler.addEventListener('hardwareBackPress', this.backHandler);
}

// backHandler = () => {
//     this.setState({backButtonEnabled:true});
//     if(this.state.backButtonEnabled) {
//         // alert(this.state.ref);
//         this.state.ref.goBack();
//         return true;
//     }
// }
    render() {
        return (
            <View style={{flex:1,marginTop:20}}>
                <WebView
                userAgent="Chrome/51.0.2704.103 Safari/537.36"
                allowFileAccess={true}
                allowUniversalAccessFromFileURLs={true}
                scalesPageToFit
                javaScriptEnabled={true}
                domStorageEnabled
                startInLoadingState
                mixedContentMode="always"
                // ref={(webView) => { this.state.ref = webView; }}
                source={{uri: 'https://www.squarecard.in/builder'}}
                style={{paddingTop:20}}
                />
            </View>
        )
    }
}

export default Dashboard;
