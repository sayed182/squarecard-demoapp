import React from "react";
import { View, Text,TextInput,Button, WebView } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

class DetailScreen extends React.Component {
  static navigationOptions = {
    title: "Card Viewer"
}
  constructor(props){
  super(props);
  this.state= {
    url:"",
    code:"",
  }
}
  getFromApi = async (code) => {
    return fetch('https://squarecard.in/api/userurl/'+code)
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.status == 200){
          this.setState({url: responseJson.url})
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }
  viewCard = ()=>{
    if(this.state.code != ""){
      this.getFromApi(this.state.code);
    }
  }
  componentDidMount(){
    
  }
  render() {
    if(this.state.url == ""){
    return (
      <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
        <Text>{this.state.url}</Text>
        <TextInput
        style={{height: 40,width:300, marginBottom:20, borderColor: 'gray', borderWidth: 1, padding:10}}
        onChangeText={(code) => this.setState({code})}
        placeholder="Enter the event code"
        value={this.state.code}
      />
      <Button
        title="View Card"
        style={{width:300}}
        onPress={this.viewCard }
      />
      </View>
    );
    }else{
      return(
        <WebView
        userAgent="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"
        allowFileAccess
        javaScriptEnabled
        // ref={(webView) => { this.state.ref = webView; }}
        source={{uri: 'https://www.squarecard.in/'+this.state.url}}
        // style={{marginTop: 20}}
        />
      );
    }
  }
}

export default DetailScreen;